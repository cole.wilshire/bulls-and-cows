/*This is the console executable, that makes use of the BullCow class
This acts as the view in the MVC pattern, and is responsible for all
user interaction. For game logic see the FBullCowGame class.
*/

//Pre-processor directives
#pragma once
#include <iostream>
#include <string>
#include "fBullCowGame.h" //Use quotations for user defined headers

//Substitutions to make syntax fit Unreal
using FText = std::string;
using int32 = int;

//Function Prototypes/Declarations
void PrintIntro();
void PlayGame();
FText GetValidGuess();
void PrintGameSummary();
bool AskToPlayAgain();

FBullCowGame BCGame; //Instantiate (create new instance of) a new game, which we re-use across plays

//Entry point of application
int main()
{
	bool bPlayAgain = false;
	do
	{
		PrintIntro();
		PlayGame();
		bPlayAgain = AskToPlayAgain(); //Takes the return value of AskToPlayAgain as its value
	}
	while (bPlayAgain);
	return 0; //Exit application
}

void PrintIntro() //Function definition
{
	std::cout << "\n\nWelcome to Bulls and Cows, a fun word game.\n"; //character output = std::cout
	std::cout << "Can you guess the " << BCGame.GetHiddenWordLength();
	std::cout << " letter isogram I'm thinking of?\n";
	std::cout << std::endl;
	return; //Not required for a Void, as voids have no return value
}

//Plays a single game to completion
void PlayGame()
{
	BCGame.Reset();
	int32 MaxTries = BCGame.GetMaxTries();
	
	//Loop asking for guesses while the game
	//is NOT won and there are still tries remaining
	while(!BCGame.IsGameWon() && BCGame.GetCurrentTry() <= MaxTries)
	{
		FText Guess = GetValidGuess();

		//Submit valid guess to the game
		FBullCowCount BullCowCount = BCGame.SubmitValidGuess(Guess);

		std::cout << "Bulls = " << BullCowCount.Bulls;
		std::cout << ". Cows = " << BullCowCount.Cows << "\n\n";
	}

	PrintGameSummary();
	return;
}

//Loop continually until user gives valid guess
FText GetValidGuess()
{
	FText Guess = "";
	EGuessStatus Status = EGuessStatus::Invalid_Status;
	do
	{
		//Get a guess from the player
		int32 CurrentTry = BCGame.GetCurrentTry();
		std::cout << "Try " << CurrentTry << " of " << BCGame.GetMaxTries();
		std::cout << ". Enter your guess: ";
		std::getline(std::cin, Guess); //Takes all of the string up to the newline cahractere (\n)

		//Check validity
		Status = BCGame.CheckGuessValidity(Guess);
		switch (Status)
		{
		case EGuessStatus::Wrong_Length:
			std::cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letter word.\n\n";
			break;
		case EGuessStatus::Not_Isogram:
			std::cout << "Please use an isogram (a word that has no repeating letters).\n\n";
			break;
		case EGuessStatus::Not_Lowercase:
			std::cout << "Please use only lower case letters.\n\n";
			break;
		default:
			//Assume the guess is valid
			break;
		}
	} while (Status != EGuessStatus::OK); //Keep looping until no errors
	return Guess;
}

void PrintGameSummary()
{
	if (BCGame.IsGameWon())
	{
		std::cout << "Congrats, you have won!\n";
	}
	else
	{
		std::cout << "Better luck next time!\n";
	}
	return;
}

bool AskToPlayAgain()
{
	std::cout << "Do you want to play again with the same hidden word? (y/n) ";
	FText Response = "";
	std::getline(std::cin, Response);
	return (Response[0] == 'y') || (Response[0] == 'Y');
}
