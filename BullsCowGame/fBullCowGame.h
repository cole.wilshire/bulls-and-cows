/*The game logic (no view code or direct user interaction
The game is a simle guess the word game based on mastermind
*/

#pragma once
#include <string>

//Makes syntax Unreal-friendly
using FString = std::string;
using int32 = int;

//All values initialized to 0
struct FBullCowCount //Structs are a class, except all of their members are public by default
{
	int32 Bulls = 0;
	int32 Cows = 0;
};

//enumerations give a variety of optional return values, defined below
enum class EGuessStatus //enum classes are a stringly-typed enum, so there values only apply to that one enum class, and not all enums gobally
{
	Invalid_Status,
	OK,
	Not_Isogram,
	Wrong_Length,
	Not_Lowercase
};

class FBullCowGame {
public: //Should only include functions, no numbers
	FBullCowGame(); //Constructor

	int32 GetMaxTries() const;
	int32 GetCurrentTry() const;
	int32 GetHiddenWordLength() const;
	bool IsGameWon() const;
	EGuessStatus CheckGuessValidity(FString) const;

	void Reset();
	FBullCowCount SubmitValidGuess(FString);

private:
	int32 MyCurrentTry; //See constructor for initialisation
	int32 MyMaxTries;
	FString MyHiddenWord;
	bool bGameIsWon;

	bool IsIsogram(FString) const;
	bool IsLowercase(FString) const;
};